# Posts Edit Form
  - update checkbox name
  - add enctype to form

# Posts Update Route
  - add upload.array()


# Posts Update Method
  - find the post by id
  - check if there's any images for deletion
    - assign deleteImages from req.body to its own variable
    - loop over deleteImages
      - delete images from cloudinary
      - delete image from post.images
    - check if there are any new images for uplaod
      - upload images
        - add  images to post.images array
    - update the post with new any new properties
    - save the updated post into the db
    - redirect to show page